# Autogenerated from the doom-fairy-floss theme

const _FAIRY_FLOSS_FACES = [
  :default => Face(height = 181, weight = :regular, slant = :normal, foreground = 0xf8f8f0, background = 0x5a5475),
  :cursor => Face(background = 0xc5a3ff),
  :black => Face(foreground = 0x5a5475),
  :red => Face(foreground = 0xcc6666),
  :green => Face(foreground = 0xc2ffdf),
  :yellow => Face(foreground = 0xffea00),
  :blue => Face(foreground = 0x55b3cc),
  :magenta => Face(foreground = 0xffb8d1),
  :cyan => Face(foreground = 0x96cbfe),
  :white => Face(foreground = 0xf8f8f0),
  :bright_black => Face(foreground = 0x464258),
  :bright_red => Face(foreground = 0xd37c7c),
  :bright_green => Face(foreground = 0xcbffe3),
  :bright_yellow => Face(foreground = 0xffed26),
  :bright_blue => Face(foreground = 0x6ebed3),
  :bright_magenta => Face(foreground = 0xffc2d7),
  :bright_cyan => Face(foreground = 0xa5d2fe),
  :bright_white => Face(foreground = 0x716799),
  :shadow => Face(foreground = 0xb8a2ce),
  :region => Face(background = 0x464258),
  :emphasis => Face(foreground = 0xc5a3ff),
  :highlight => Face(foreground = 0x464258, background = 0xc5a3ff),
  :code => Face(foreground = 0xc2ffdf),
  :error => Face(foreground = 0xcc6666),
  :warning => Face(foreground = 0xffea00),
  :success => Face(foreground = 0xc2ffdf),
  :info => Face(),
  :note => Face(),
  :tip => Face(),
  :julia_funcall => Face(foreground = 0xc2ffdf),
  :julia_identifier => Face(),
  :julia_macro => Face(foreground = 0xe6c000, inherit = [:markdown_admonition]),
  :julia_symbol => Face(foreground = 0xc5a3ff),
  :julia_nothing => Face(foreground = 0xc5a3ff),
  :julia_type => Face(foreground = 0x96cbfe),
  :julia_comment => Face(foreground = 0xe6c000),
  :julia_string => Face(foreground = 0xffea00),
  :julia_string_delim => Face(foreground = 0xffea00),
  :julia_cmdstring => Face(foreground = 0xffea00),
  :julia_char => Face(foreground = 0xffea00),
  :julia_char_delim => Face(foreground = 0xffea00),
  :julia_number => Face(foreground = 0xc5a3ff, inherit = [:markdown_admonition]),
  :julia_bool => Face(foreground = 0xc5a3ff),
  :julia_operator => Face(foreground = 0x96cbfe),
  :julia_comparator => Face(foreground = 0x96cbfe),
  :julia_assignment => Face(foreground = 0x96cbfe),
  :julia_keyword => Face(slant = :italic, foreground = 0x96cbfe),
  :julia_error => Face(weight = :bold, foreground = 0xcc6666, inverse = true),
  :julia_parenthetical => Face(inherit = [:default]),
  :julia_rainbow_paren_1 => Face(foreground = 0x55b3cc),
  :julia_rainbow_paren_2 => Face(foreground = 0xffb8d1),
  :julia_rainbow_paren_3 => Face(foreground = 0xc2ffdf),
  :julia_rainbow_paren_4 => Face(foreground = 0xc5a3ff),
  :julia_rainbow_paren_5 => Face(foreground = 0x8295d6),
  :julia_rainbow_paren_6 => Face(foreground = 0x55b3cc),
  :julia_rainbow_bracket_1 => Face(foreground = 0x55b3cc),
  :julia_rainbow_bracket_2 => Face(foreground = 0xffb8d1),
  :julia_rainbow_bracket_3 => Face(foreground = 0xc2ffdf),
  :julia_rainbow_bracket_4 => Face(foreground = 0xc5a3ff),
  :julia_rainbow_bracket_5 => Face(foreground = 0x8295d6),
  :julia_rainbow_bracket_6 => Face(foreground = 0x55b3cc),
  :julia_rainbow_curly_1 => Face(foreground = 0x55b3cc),
  :julia_rainbow_curly_2 => Face(foreground = 0xffb8d1),
  :julia_rainbow_curly_3 => Face(foreground = 0xc2ffdf),
  :julia_rainbow_curly_4 => Face(foreground = 0xc5a3ff),
  :julia_rainbow_curly_5 => Face(foreground = 0x8295d6),
  :julia_rainbow_curly_6 => Face(foreground = 0x55b3cc),
  :markdown_header => Face(),
  :markdown_h1 => Face(height = 1.25, weight = :extrabold, foreground = 0xe6c000),
  :markdown_h2 => Face(height = 1.15, weight = :bold, foreground = 0xc2ffdf),
  :markdown_h3 => Face(height = 1.12, weight = :bold, foreground = 0x8295d6),
  :markdown_h4 => Face(height = 1.09, weight = :semibold, foreground = 0xb89900),
  :markdown_h5 => Face(height = 1.06, weight = :semibold, foreground = 0x9bccb2),
  :markdown_h6 => Face(height = 1.03, weight = :semibold, foreground = 0x6877ab),
  :markdown_admonition => Face(weight = :bold),
  :markdown_code => Face(background = 0x9673d3),
  :markdown_footnote => Face(inherit = [:nil]),
  :markdown_hrule => Face(inherit = [:nil]),
  :markdown_inlinecode => Face(inherit = [:markdown_code, :nil]),
  :markdown_latex => Face(background = 0x9673d3),
  :markdown_link => Face(foreground = 0xc5a3ff),
  :markdown_list => Face(foreground = 0xcc6666),
]

THEMES[:fairy_floss] = _FAIRY_FLOSS_FACES
