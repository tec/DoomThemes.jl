# Autogenerated from the doom-tokyo-night theme

const _TOKYO_NIGHT_FACES = [
  :default => Face(height = 181, weight = :regular, slant = :normal, foreground = 0xa9b1d6, background = 0x1a1b26),
  :cursor => Face(background = 0xb4f9f8),
  :black => Face(foreground = 0x1a1b26),
  :red => Face(foreground = 0xf7768e),
  :green => Face(foreground = 0x73daca),
  :yellow => Face(foreground = 0xe0af68),
  :blue => Face(foreground = 0x7aa2f7),
  :magenta => Face(foreground = 0xbb9af7),
  :cyan => Face(foreground = 0xb4f9f8),
  :white => Face(foreground = 0xa9b1d6),
  :bright_black => Face(foreground = 0x414868),
  :bright_red => Face(foreground = 0xf88a9e),
  :bright_green => Face(foreground = 0x88dfd1),
  :bright_yellow => Face(foreground = 0xe4bb7e),
  :bright_blue => Face(foreground = 0x8daff8),
  :bright_magenta => Face(foreground = 0xc5a9f8),
  :bright_cyan => Face(foreground = 0xbff9f9),
  :bright_white => Face(foreground = 0xc0caf5),
  :shadow => Face(foreground = 0x9099c0),
  :region => Face(background = 0x414868),
  :emphasis => Face(foreground = 0xb4f9f8),
  :highlight => Face(foreground = 0x414868, background = 0xb4f9f8),
  :code => Face(foreground = 0x7aa2f7),
  :error => Face(foreground = 0xf7768e),
  :warning => Face(foreground = 0xe0af68),
  :success => Face(foreground = 0x73daca),
  :info => Face(),
  :note => Face(),
  :tip => Face(),
  :julia_funcall => Face(foreground = 0x7aa2f7),
  :julia_identifier => Face(),
  :julia_macro => Face(foreground = 0x7dcfff, inherit = [:markdown_admonition]),
  :julia_symbol => Face(foreground = 0xff9e64),
  :julia_nothing => Face(foreground = 0xff9e64),
  :julia_type => Face(foreground = 0xc0caf5),
  :julia_comment => Face(foreground = 0x51587a),
  :julia_string => Face(foreground = 0x9ece6a),
  :julia_string_delim => Face(foreground = 0x9ece6a),
  :julia_cmdstring => Face(foreground = 0x9ece6a),
  :julia_char => Face(foreground = 0x9ece6a),
  :julia_char_delim => Face(foreground = 0x9ece6a),
  :julia_number => Face(foreground = 0xff9e64, inherit = [:markdown_admonition]),
  :julia_bool => Face(foreground = 0xff9e64),
  :julia_operator => Face(foreground = 0xc0caf5),
  :julia_comparator => Face(foreground = 0xc0caf5),
  :julia_assignment => Face(foreground = 0xc0caf5),
  :julia_keyword => Face(foreground = 0xbb9af7),
  :julia_error => Face(weight = :bold, foreground = 0xf7768e, inverse = true),
  :julia_parenthetical => Face(inherit = [:default]),
  :julia_rainbow_paren_1 => Face(foreground = 0xa9b1d6),
  :julia_rainbow_paren_2 => Face(foreground = 0x7aa2f7),
  :julia_rainbow_paren_3 => Face(foreground = 0xff9e64),
  :julia_rainbow_paren_4 => Face(foreground = 0x73daca),
  :julia_rainbow_paren_5 => Face(foreground = 0xb4f9f8),
  :julia_rainbow_paren_6 => Face(foreground = 0xe0af68),
  :julia_rainbow_bracket_1 => Face(foreground = 0xa9b1d6),
  :julia_rainbow_bracket_2 => Face(foreground = 0x7aa2f7),
  :julia_rainbow_bracket_3 => Face(foreground = 0xff9e64),
  :julia_rainbow_bracket_4 => Face(foreground = 0x73daca),
  :julia_rainbow_bracket_5 => Face(foreground = 0xb4f9f8),
  :julia_rainbow_bracket_6 => Face(foreground = 0xe0af68),
  :julia_rainbow_curly_1 => Face(foreground = 0xa9b1d6),
  :julia_rainbow_curly_2 => Face(foreground = 0x7aa2f7),
  :julia_rainbow_curly_3 => Face(foreground = 0xff9e64),
  :julia_rainbow_curly_4 => Face(foreground = 0x73daca),
  :julia_rainbow_curly_5 => Face(foreground = 0xb4f9f8),
  :julia_rainbow_curly_6 => Face(foreground = 0xe0af68),
  :markdown_header => Face(),
  :markdown_h1 => Face(height = 1.25, weight = :extrabold, foreground = 0x7aa2f7),
  :markdown_h2 => Face(height = 1.15, weight = :bold, foreground = 0xbb9af7),
  :markdown_h3 => Face(height = 1.12, weight = :bold, foreground = 0x9aa5ce),
  :markdown_h4 => Face(height = 1.09, weight = :semibold, foreground = 0x9bb9f9),
  :markdown_h5 => Face(height = 1.06, weight = :semibold, foreground = 0xcbb3f9),
  :markdown_h6 => Face(height = 1.03, weight = :semibold, foreground = 0xbcd0fb),
  :markdown_admonition => Face(weight = :bold),
  :markdown_code => Face(foreground = 0x7dcfff, background = 0x23242e),
  :markdown_footnote => Face(inherit = [:nil]),
  :markdown_hrule => Face(inherit = [:nil]),
  :markdown_inlinecode => Face(inherit = [:markdown_code, :nil]),
  :markdown_latex => Face(foreground = 0x7dcfff, background = 0x23242e),
  :markdown_link => Face(foreground = 0xb4f9f8),
  :markdown_list => Face(foreground = 0xf7768e),
]

THEMES[:tokyo_night] = _TOKYO_NIGHT_FACES
