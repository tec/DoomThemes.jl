# Autogenerated from the doom-tomorrow-day theme

const _TOMORROW_DAY_FACES = [
  :default => Face(height = 181, weight = :regular, slant = :normal, foreground = 0x4d4d4c, background = 0xffffff),
  :cursor => Face(background = 0x4271ae),
  :black => Face(foreground = 0xffffff),
  :red => Face(foreground = 0xc82829),
  :green => Face(foreground = 0x718c00),
  :yellow => Face(foreground = 0xeab700),
  :blue => Face(foreground = 0x4271ae),
  :magenta => Face(foreground = 0xc678dd),
  :cyan => Face(foreground = 0x8abeb7),
  :white => Face(foreground = 0x4d4d4c),
  :bright_black => Face(foreground = 0xf2f2f2),
  :bright_red => Face(foreground = 0xd04849),
  :bright_green => Face(foreground = 0x869d26),
  :bright_yellow => Face(foreground = 0xedc126),
  :bright_blue => Face(foreground = 0x5e86ba),
  :bright_magenta => Face(foreground = 0xce8ce2),
  :bright_cyan => Face(foreground = 0x9bc7c1),
  :bright_white => Face(foreground = 0x000000),
  :shadow => Face(foreground = 0xa3a1a1),
  :region => Face(background = 0xe4e4e4),
  :emphasis => Face(foreground = 0x4271ae),
  :highlight => Face(foreground = 0xf2f2f2, background = 0x4271ae),
  :code => Face(foreground = 0x4271ae),
  :error => Face(foreground = 0xc82829),
  :warning => Face(foreground = 0xeab700),
  :success => Face(foreground = 0x718c00),
  :info => Face(),
  :note => Face(),
  :tip => Face(),
  :julia_funcall => Face(foreground = 0x4271ae),
  :julia_identifier => Face(),
  :julia_macro => Face(foreground = 0x4d4d4c, inherit = [:markdown_admonition]),
  :julia_symbol => Face(foreground = 0xf5871f),
  :julia_nothing => Face(foreground = 0xf5871f),
  :julia_type => Face(foreground = 0xbb9200),
  :julia_comment => Face(foreground = 0x8e908c),
  :julia_string => Face(foreground = 0x718c00),
  :julia_string_delim => Face(foreground = 0x718c00),
  :julia_cmdstring => Face(foreground = 0x718c00),
  :julia_char => Face(foreground = 0x718c00),
  :julia_char_delim => Face(foreground = 0x718c00),
  :julia_number => Face(foreground = 0xf5871f, inherit = [:markdown_admonition]),
  :julia_bool => Face(foreground = 0xf5871f),
  :julia_operator => Face(foreground = 0xbb9200),
  :julia_comparator => Face(foreground = 0xbb9200),
  :julia_assignment => Face(foreground = 0xbb9200),
  :julia_keyword => Face(foreground = 0x8959a8),
  :julia_error => Face(weight = :bold, foreground = 0xc82829, inverse = true),
  :julia_parenthetical => Face(inherit = [:default]),
  :julia_rainbow_paren_1 => Face(foreground = 0x8959a8),
  :julia_rainbow_paren_2 => Face(foreground = 0x4271ae),
  :julia_rainbow_paren_3 => Face(foreground = 0x718c00),
  :julia_rainbow_paren_4 => Face(foreground = 0xc678dd),
  :julia_rainbow_paren_5 => Face(foreground = 0xf5871f),
  :julia_rainbow_paren_6 => Face(foreground = 0xeab700),
  :julia_rainbow_bracket_1 => Face(foreground = 0x8959a8),
  :julia_rainbow_bracket_2 => Face(foreground = 0x4271ae),
  :julia_rainbow_bracket_3 => Face(foreground = 0x718c00),
  :julia_rainbow_bracket_4 => Face(foreground = 0xc678dd),
  :julia_rainbow_bracket_5 => Face(foreground = 0xf5871f),
  :julia_rainbow_bracket_6 => Face(foreground = 0xeab700),
  :julia_rainbow_curly_1 => Face(foreground = 0x8959a8),
  :julia_rainbow_curly_2 => Face(foreground = 0x4271ae),
  :julia_rainbow_curly_3 => Face(foreground = 0x718c00),
  :julia_rainbow_curly_4 => Face(foreground = 0xc678dd),
  :julia_rainbow_curly_5 => Face(foreground = 0xf5871f),
  :julia_rainbow_curly_6 => Face(foreground = 0xeab700),
  :markdown_header => Face(),
  :markdown_h1 => Face(height = 1.25, weight = :extrabold, foreground = 0x3e999f),
  :markdown_h2 => Face(height = 1.15, weight = :bold, foreground = 0x4271ae),
  :markdown_h3 => Face(height = 1.12, weight = :bold, foreground = 0x8959a8),
  :markdown_h4 => Face(height = 1.09, weight = :semibold, foreground = 0x4271ae),
  :markdown_h5 => Face(height = 1.06, weight = :semibold, foreground = 0x8959a8),
  :markdown_h6 => Face(height = 1.03, weight = :semibold, foreground = 0x4271ae),
  :markdown_admonition => Face(weight = :bold),
  :markdown_code => Face(background = 0xd6d4d4),
  :markdown_footnote => Face(inherit = [:nil]),
  :markdown_hrule => Face(inherit = [:nil]),
  :markdown_inlinecode => Face(inherit = [:markdown_code, :nil]),
  :markdown_latex => Face(background = 0xd6d4d4),
  :markdown_link => Face(foreground = 0x4271ae),
  :markdown_list => Face(foreground = 0xc82829),
]

THEMES[:tomorrow_day] = _TOMORROW_DAY_FACES
