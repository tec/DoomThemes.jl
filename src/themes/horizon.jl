# Autogenerated from the doom-horizon theme

const _HORIZON_FACES = [
  :default => Face(height = 181, weight = :regular, slant = :normal, foreground = 0xc7c9cb, background = 0x232530),
  :cursor => Face(background = 0xe95678),
  :black => Face(foreground = 0x232530),
  :red => Face(foreground = 0xe95678),
  :green => Face(foreground = 0x09f7a0),
  :yellow => Face(foreground = 0xfab795),
  :blue => Face(foreground = 0x21bfc2),
  :magenta => Face(foreground = 0x6c6f93),
  :cyan => Face(foreground = 0x59e3e3),
  :white => Face(foreground = 0xc7c9cb),
  :bright_black => Face(foreground = 0x16161c),
  :bright_red => Face(foreground = 0xec6f8c),
  :bright_green => Face(foreground = 0x2df8ae),
  :bright_yellow => Face(foreground = 0xfac1a4),
  :bright_blue => Face(foreground = 0x42c8cb),
  :bright_magenta => Face(foreground = 0x8284a3),
  :bright_cyan => Face(foreground = 0x71e7e7),
  :bright_white => Face(foreground = 0xfdf0ed),
  :shadow => Face(foreground = 0xf9cec3),
  :region => Face(background = 0x2e2f3a),
  :emphasis => Face(foreground = 0xe95678),
  :highlight => Face(foreground = 0x16161c, background = 0xe95678),
  :code => Face(foreground = 0x87ceeb),
  :error => Face(foreground = 0xe95678),
  :warning => Face(foreground = 0x27d797),
  :success => Face(foreground = 0x09f7a0),
  :info => Face(),
  :note => Face(),
  :tip => Face(),
  :julia_funcall => Face(foreground = 0x87ceeb),
  :julia_identifier => Face(),
  :julia_macro => Face(foreground = 0x87ceeb, inherit = [:markdown_admonition]),
  :julia_symbol => Face(foreground = 0xf09383),
  :julia_nothing => Face(foreground = 0xf09383),
  :julia_type => Face(foreground = 0x87ceeb),
  :julia_comment => Face(slant = :italic, foreground = 0x4e5059),
  :julia_string => Face(foreground = 0xfab795),
  :julia_string_delim => Face(foreground = 0xfab795),
  :julia_cmdstring => Face(foreground = 0xfab795),
  :julia_char => Face(foreground = 0xfab795),
  :julia_char_delim => Face(foreground = 0xfab795),
  :julia_number => Face(foreground = 0xf09383, inherit = [:markdown_admonition]),
  :julia_bool => Face(foreground = 0xf09383),
  :julia_operator => Face(foreground = 0x87ceeb),
  :julia_comparator => Face(foreground = 0x87ceeb),
  :julia_assignment => Face(foreground = 0x87ceeb),
  :julia_keyword => Face(foreground = 0xb877db),
  :julia_error => Face(weight = :bold, foreground = 0xe95678, inverse = true),
  :julia_parenthetical => Face(inherit = [:default]),
  :julia_rainbow_paren_1 => Face(foreground = 0x21bfc2),
  :julia_rainbow_paren_2 => Face(foreground = 0x6c6f93),
  :julia_rainbow_paren_3 => Face(foreground = 0x09f7a0),
  :julia_rainbow_paren_4 => Face(foreground = 0xb877db),
  :julia_rainbow_paren_5 => Face(foreground = 0x87ceeb),
  :julia_rainbow_paren_6 => Face(foreground = 0x21bfc2),
  :julia_rainbow_bracket_1 => Face(foreground = 0x21bfc2),
  :julia_rainbow_bracket_2 => Face(foreground = 0x6c6f93),
  :julia_rainbow_bracket_3 => Face(foreground = 0x09f7a0),
  :julia_rainbow_bracket_4 => Face(foreground = 0xb877db),
  :julia_rainbow_bracket_5 => Face(foreground = 0x87ceeb),
  :julia_rainbow_bracket_6 => Face(foreground = 0x21bfc2),
  :julia_rainbow_curly_1 => Face(foreground = 0x21bfc2),
  :julia_rainbow_curly_2 => Face(foreground = 0x6c6f93),
  :julia_rainbow_curly_3 => Face(foreground = 0x09f7a0),
  :julia_rainbow_curly_4 => Face(foreground = 0xb877db),
  :julia_rainbow_curly_5 => Face(foreground = 0x87ceeb),
  :julia_rainbow_curly_6 => Face(foreground = 0x21bfc2),
  :markdown_header => Face(),
  :markdown_h1 => Face(height = 1.25, weight = :extrabold, foreground = 0x21bfc2),
  :markdown_h2 => Face(height = 1.15, weight = :bold, foreground = 0x6c6f93),
  :markdown_h3 => Face(height = 1.12, weight = :bold, foreground = 0xb877db),
  :markdown_h4 => Face(height = 1.09, weight = :semibold, foreground = 0x58cfd1),
  :markdown_h5 => Face(height = 1.06, weight = :semibold, foreground = 0x9093ae),
  :markdown_h6 => Face(height = 1.03, weight = :semibold, foreground = 0x90dfe0),
  :markdown_admonition => Face(weight = :bold),
  :markdown_code => Face(foreground = 0xf09383, background = 0x1a1c23),
  :markdown_footnote => Face(inherit = [:nil]),
  :markdown_hrule => Face(inherit = [:nil]),
  :markdown_inlinecode => Face(inherit = [:markdown_code, :nil]),
  :markdown_latex => Face(foreground = 0xf09383, background = 0x1a1c23),
  :markdown_link => Face(foreground = 0xf09383),
  :markdown_list => Face(foreground = 0xe95678),
]

THEMES[:horizon] = _HORIZON_FACES
