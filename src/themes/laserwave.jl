# Autogenerated from the doom-laserwave theme

const _LASERWAVE_FACES = [
  :default => Face(height = 181, weight = :regular, slant = :normal, foreground = 0xffffff, background = 0x27212e),
  :cursor => Face(background = 0xeb64b9),
  :black => Face(foreground = 0x27212e),
  :red => Face(foreground = 0x964c7b),
  :green => Face(foreground = 0x74dfc4),
  :yellow => Face(foreground = 0xffe261),
  :blue => Face(foreground = 0x40b4c4),
  :magenta => Face(foreground = 0xeb64b9),
  :cyan => Face(foreground = 0xb4dce7),
  :white => Face(foreground = 0xffffff),
  :bright_black => Face(foreground = 0x222228),
  :bright_red => Face(foreground = 0xa5668e),
  :bright_green => Face(foreground = 0x88e3cc),
  :bright_yellow => Face(foreground = 0xffe678),
  :bright_blue => Face(foreground = 0x5cbfcc),
  :bright_magenta => Face(foreground = 0xed7bc3),
  :bright_cyan => Face(foreground = 0xbfe1ea),
  :bright_white => Face(foreground = 0xeceff4),
  :shadow => Face(foreground = 0x544863),
  :region => Face(background = 0x4e2e49),
  :emphasis => Face(foreground = 0x1b1720),
  :highlight => Face(foreground = 0x222228, background = 0xeb64b9),
  :code => Face(foreground = 0xeb64b9),
  :error => Face(foreground = 0x964c7b),
  :warning => Face(foreground = 0xffe261),
  :success => Face(foreground = 0x74dfc4),
  :info => Face(),
  :note => Face(),
  :tip => Face(),
  :julia_funcall => Face(foreground = 0xeb64b9),
  :julia_identifier => Face(),
  :julia_macro => Face(foreground = 0x40b4c4, inherit = [:markdown_admonition]),
  :julia_symbol => Face(foreground = 0xb381c5),
  :julia_nothing => Face(foreground = 0xb381c5),
  :julia_type => Face(foreground = 0xffe261),
  :julia_comment => Face(foreground = 0x91889b),
  :julia_string => Face(foreground = 0xb4dce7),
  :julia_string_delim => Face(foreground = 0xb4dce7),
  :julia_cmdstring => Face(foreground = 0xb4dce7),
  :julia_char => Face(foreground = 0xb4dce7),
  :julia_char_delim => Face(foreground = 0xb4dce7),
  :julia_number => Face(foreground = 0xffb85b, inherit = [:markdown_admonition]),
  :julia_bool => Face(foreground = 0xb381c5),
  :julia_operator => Face(foreground = 0xffe261),
  :julia_comparator => Face(foreground = 0xffe261),
  :julia_assignment => Face(foreground = 0xffe261),
  :julia_keyword => Face(foreground = 0x40b4c4),
  :julia_error => Face(weight = :bold, foreground = 0x964c7b, inverse = true),
  :julia_parenthetical => Face(inherit = [:default]),
  :julia_rainbow_paren_1 => Face(foreground = 0x40b4c4),
  :julia_rainbow_paren_2 => Face(foreground = 0xeb64b9),
  :julia_rainbow_paren_3 => Face(foreground = 0x74dfc4),
  :julia_rainbow_paren_4 => Face(foreground = 0xb381c5),
  :julia_rainbow_paren_5 => Face(foreground = 0x4d8079),
  :julia_rainbow_paren_6 => Face(foreground = 0x40b4c4),
  :julia_rainbow_bracket_1 => Face(foreground = 0x40b4c4),
  :julia_rainbow_bracket_2 => Face(foreground = 0xeb64b9),
  :julia_rainbow_bracket_3 => Face(foreground = 0x74dfc4),
  :julia_rainbow_bracket_4 => Face(foreground = 0xb381c5),
  :julia_rainbow_bracket_5 => Face(foreground = 0x4d8079),
  :julia_rainbow_bracket_6 => Face(foreground = 0x40b4c4),
  :julia_rainbow_curly_1 => Face(foreground = 0x40b4c4),
  :julia_rainbow_curly_2 => Face(foreground = 0xeb64b9),
  :julia_rainbow_curly_3 => Face(foreground = 0x74dfc4),
  :julia_rainbow_curly_4 => Face(foreground = 0xb381c5),
  :julia_rainbow_curly_5 => Face(foreground = 0x4d8079),
  :julia_rainbow_curly_6 => Face(foreground = 0x40b4c4),
  :markdown_header => Face(),
  :markdown_h1 => Face(height = 1.25, weight = :extrabold, foreground = 0x40b4c4),
  :markdown_h2 => Face(height = 1.15, weight = :bold, foreground = 0x74dfc4),
  :markdown_h3 => Face(height = 1.12, weight = :bold, foreground = 0x4d8079),
  :markdown_h4 => Face(height = 1.09, weight = :semibold, foreground = 0x33909c),
  :markdown_h5 => Face(height = 1.06, weight = :semibold, foreground = 0x5cb29c),
  :markdown_h6 => Face(height = 1.03, weight = :semibold, foreground = 0x3d6660),
  :markdown_admonition => Face(weight = :bold),
  :markdown_code => Face(background = 0x3e3549),
  :markdown_footnote => Face(inherit = [:nil]),
  :markdown_hrule => Face(inherit = [:nil]),
  :markdown_inlinecode => Face(inherit = [:markdown_code, :nil]),
  :markdown_latex => Face(background = 0x3e3549),
  :markdown_link => Face(foreground = 0xb4dce7),
  :markdown_list => Face(foreground = 0x91889b),
]

THEMES[:laserwave] = _LASERWAVE_FACES
