# Autogenerated from the doom-acario-dark theme

const _ACARIO_DARK_FACES = [
  :default => Face(height = 181, weight = :regular, slant = :normal, foreground = 0xcedbe5, background = 0x0d0e16),
  :cursor => Face(background = 0xd85f00),
  :black => Face(foreground = 0x0d0e16),
  :red => Face(foreground = 0xd83441),
  :green => Face(foreground = 0x79d836),
  :yellow => Face(foreground = 0xd8b941),
  :blue => Face(foreground = 0x3679d8),
  :magenta => Face(foreground = 0x8041d8),
  :cyan => Face(foreground = 0x36d8bd),
  :white => Face(foreground = 0xcedbe5),
  :bright_black => Face(foreground = 0x0f1019),
  :bright_red => Face(foreground = 0xdd525d),
  :bright_green => Face(foreground = 0x8ddd54),
  :bright_yellow => Face(foreground = 0xddc35d),
  :bright_blue => Face(foreground = 0x548ddd),
  :bright_magenta => Face(foreground = 0x935ddd),
  :bright_cyan => Face(foreground = 0x54ddc6),
  :bright_white => Face(foreground = 0xd0d0d0),
  :shadow => Face(foreground = 0x767676),
  :region => Face(background = 0x1e1e33),
  :emphasis => Face(foreground = 0xd85f00),
  :highlight => Face(foreground = 0x0f1019, background = 0xd85f00),
  :code => Face(foreground = 0xd8b941),
  :error => Face(foreground = 0xd83441),
  :warning => Face(foreground = 0xd85f00),
  :success => Face(foreground = 0x79d836),
  :info => Face(),
  :note => Face(),
  :tip => Face(),
  :julia_funcall => Face(foreground = 0xd8b941),
  :julia_identifier => Face(),
  :julia_macro => Face(foreground = 0x3679d8, inherit = [:markdown_admonition]),
  :julia_symbol => Face(foreground = 0x8041d8),
  :julia_nothing => Face(foreground = 0x8041d8),
  :julia_type => Face(foreground = 0x3679d8),
  :julia_comment => Face(slant = :italic, foreground = 0x767676),
  :julia_string => Face(foreground = 0x79d836),
  :julia_string_delim => Face(foreground = 0x79d836),
  :julia_cmdstring => Face(foreground = 0x79d836),
  :julia_char => Face(foreground = 0x79d836),
  :julia_char_delim => Face(foreground = 0x79d836),
  :julia_number => Face(foreground = 0xd85f00, inherit = [:markdown_admonition]),
  :julia_bool => Face(foreground = 0x8041d8),
  :julia_operator => Face(foreground = 0x3679d8),
  :julia_comparator => Face(foreground = 0x3679d8),
  :julia_assignment => Face(foreground = 0x3679d8),
  :julia_keyword => Face(foreground = 0xd83441),
  :julia_error => Face(weight = :bold, foreground = 0xd83441, inverse = true),
  :julia_parenthetical => Face(inherit = [:default]),
  :julia_rainbow_paren_1 => Face(foreground = 0x3679d8),
  :julia_rainbow_paren_2 => Face(foreground = 0x8041d8),
  :julia_rainbow_paren_3 => Face(foreground = 0x79d836),
  :julia_rainbow_paren_4 => Face(foreground = 0xab11d8),
  :julia_rainbow_paren_5 => Face(foreground = 0x2d9574),
  :julia_rainbow_paren_6 => Face(foreground = 0x3679d8),
  :julia_rainbow_bracket_1 => Face(foreground = 0x3679d8),
  :julia_rainbow_bracket_2 => Face(foreground = 0x8041d8),
  :julia_rainbow_bracket_3 => Face(foreground = 0x79d836),
  :julia_rainbow_bracket_4 => Face(foreground = 0xab11d8),
  :julia_rainbow_bracket_5 => Face(foreground = 0x2d9574),
  :julia_rainbow_bracket_6 => Face(foreground = 0x3679d8),
  :julia_rainbow_curly_1 => Face(foreground = 0x3679d8),
  :julia_rainbow_curly_2 => Face(foreground = 0x8041d8),
  :julia_rainbow_curly_3 => Face(foreground = 0x79d836),
  :julia_rainbow_curly_4 => Face(foreground = 0xab11d8),
  :julia_rainbow_curly_5 => Face(foreground = 0x2d9574),
  :julia_rainbow_curly_6 => Face(foreground = 0x3679d8),
  :markdown_header => Face(),
  :markdown_h1 => Face(height = 1.25, weight = :extrabold, foreground = 0x3679d8),
  :markdown_h2 => Face(height = 1.15, weight = :bold, foreground = 0x8041d8),
  :markdown_h3 => Face(height = 1.12, weight = :bold, foreground = 0xab11d8),
  :markdown_h4 => Face(height = 1.09, weight = :semibold, foreground = 0x689ae1),
  :markdown_h5 => Face(height = 1.06, weight = :semibold, foreground = 0x9f70e1),
  :markdown_h6 => Face(height = 1.03, weight = :semibold, foreground = 0x9abceb),
  :markdown_admonition => Face(weight = :bold),
  :markdown_code => Face(background = 0x4f535e),
  :markdown_footnote => Face(inherit = [:nil]),
  :markdown_hrule => Face(inherit = [:nil]),
  :markdown_inlinecode => Face(inherit = [:markdown_code, :nil]),
  :markdown_latex => Face(background = 0x4f535e),
  :markdown_link => Face(foreground = 0xd85f00),
  :markdown_list => Face(foreground = 0xd83441),
]

THEMES[:acario_dark] = _ACARIO_DARK_FACES
