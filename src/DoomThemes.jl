module DoomThemes

using StyledStrings: Face, getface, resetfaces!, loadface!,
    withfaces, SimpleColor, RGBTuple, @styled_str

const THEMES = Dict{Symbol, Vector{Pair{Symbol, Face}}}()

@eval for theme in readdir(joinpath(@__DIR__, "themes"))
    include("themes/$theme")
end

function list()
    for theme in sort(keys(THEMES) |> collect)
        withfaces(THEMES[theme]) do
            println(styled" $(rpad(theme, 30))   \
                           {emphasis:■} {julia_funcall:■} {julia_symbol:■} {julia_type:■} {julia_string:■}   \
                           {(fg=red):■} {(fg=green):■} {(fg=yellow):■} {(fg=blue):■} \
                           {(fg=magenta):■} {(fg=cyan):■} {(fg=white):■} {(fg=black):■}")
        end
    end
end

function load!(theme::Symbol, term::Bool=true)
    faces = get(THEMES, theme, nothing)
    isnothing(faces) && throw(ArgumentError("The doom theme $theme is not defined."))
    resetfaces!()
    foreach(loadface!, faces)
    term && theme_terminal!(faces)
end

include("termtheme.jl")

function __init__()
    if isinteractive()
        atexit(() -> reset!(true))
    end
end

end
