;;; make-theme.el --- Load a theme and write a def file -*- lexical-binding: t; -*-

(require 'doom-themes)
(require 'outline)
(require 'markdown-mode)
(require 'rainbow-delimiters)

(defvar make-jl-doom-theme--face-mapping
  '((default                           . "default")
    ;; Extra
    (cursor                            . "cursor")
    ;; Basic faces
    (ansi-color-black                  . "black")
    (ansi-color-red                    . "red")
    (ansi-color-green                  . "green")
    (ansi-color-yellow                 . "yellow")
    (ansi-color-blue                   . "blue")
    (ansi-color-magenta                . "magenta")
    (ansi-color-cyan                   . "cyan")
    (ansi-color-white                  . "white")
    (ansi-color-bright-black           . "bright_black")
    (ansi-color-bright-red             . "bright_red")
    (ansi-color-bright-green           . "bright_green")
    (ansi-color-bright-yellow          . "bright_yellow")
    (ansi-color-bright-blue            . "bright_blue")
    (ansi-color-bright-magenta         . "bright_magenta")
    (ansi-color-bright-cyan            . "bright_cyan")
    (ansi-color-bright-white           . "bright_white")
    (shadow                            . "shadow")
    (region                            . "region")
    (mode-line-emphasis                . "emphasis")
    (highlight                         . "highlight")
    (font-lock-function-name-face      . "code")
    (error                             . "error")
    (warning                           . "warning")
    (success                           . "success")
    (nil                               . "info")
    (nil                               . "note")
    (nil                               . "tip")
    ;; Syntax highlighting
    (font-lock-function-name-face      . "julia_funcall")
    (nil                               . "julia_identifier")
    (font-lock-preprocessor-face       . "julia_macro")
    (font-lock-constant-face           . "julia_symbol")
    (font-lock-constant-face           . "julia_nothing")
    (font-lock-type-face               . "julia_type")
    (font-lock-comment-face            . "julia_comment")
    (font-lock-string-face             . "julia_string")
    (font-lock-string-face             . "julia_string_delim")
    (font-lock-string-face             . "julia_cmdstring")
    (font-lock-string-face             . "julia_char")
    (font-lock-string-face             . "julia_char_delim")
    (highlight-numbers-number          . "julia_number")
    (font-lock-constant-face           . "julia_bool")
    (font-lock-type-face               . "julia_operator")
    (font-lock-type-face               . "julia_comparator")
    (font-lock-type-face               . "julia_assignment")
    (font-lock-keyword-face            . "julia_keyword")
    (rainbow-delimiters-unmatched-face . "julia_error")
    ;; Syntax highlighting - delimiters
    (rainbow-delimiters-base-face      . "julia_parenthetical")
    (rainbow-delimiters-depth-1-face   . "julia_rainbow_paren_1")
    (rainbow-delimiters-depth-2-face   . "julia_rainbow_paren_2")
    (rainbow-delimiters-depth-3-face   . "julia_rainbow_paren_3")
    (rainbow-delimiters-depth-4-face   . "julia_rainbow_paren_4")
    (rainbow-delimiters-depth-5-face   . "julia_rainbow_paren_5")
    (rainbow-delimiters-depth-6-face   . "julia_rainbow_paren_6")
    (rainbow-delimiters-depth-1-face   . "julia_rainbow_bracket_1")
    (rainbow-delimiters-depth-2-face   . "julia_rainbow_bracket_2")
    (rainbow-delimiters-depth-3-face   . "julia_rainbow_bracket_3")
    (rainbow-delimiters-depth-4-face   . "julia_rainbow_bracket_4")
    (rainbow-delimiters-depth-5-face   . "julia_rainbow_bracket_5")
    (rainbow-delimiters-depth-6-face   . "julia_rainbow_bracket_6")
    (rainbow-delimiters-depth-1-face   . "julia_rainbow_curly_1")
    (rainbow-delimiters-depth-2-face   . "julia_rainbow_curly_2")
    (rainbow-delimiters-depth-3-face   . "julia_rainbow_curly_3")
    (rainbow-delimiters-depth-4-face   . "julia_rainbow_curly_4")
    (rainbow-delimiters-depth-5-face   . "julia_rainbow_curly_5")
    (rainbow-delimiters-depth-6-face   . "julia_rainbow_curly_6")
    ;; Markdown
    (nil                               . "markdown_header")
    (outline-1                         . "markdown_h1")
    (outline-2                         . "markdown_h2")
    (outline-3                         . "markdown_h3")
    (outline-4                         . "markdown_h4")
    (outline-5                         . "markdown_h5")
    (outline-6                         . "markdown_h6")
    (bold                              . "markdown_admonition")
    (markdown-code-face                . "markdown_code")
    (markdown-footnote-marker-face     . "markdown_footnote")
    (markdown-hr-face                  . "markdown_hrule")
    (markdown-inline-code-face         . "markdown_inlinecode")
    (markdown-code-face                . "markdown_latex")
    (markdown-link-face                . "markdown_link")
    (markdown-list-face                . "markdown_list")))

(defvar make-jl-doom-themes--all
  '(1337 Iosvkem acario-dark acario-light ayu-dark ayu-light ayu-mirage badger
    challenger-deep city-lights dark+ dracula earl-grey ephemeral fairy-floss
    feather-dark feather-light flatwhite gruvbox-light gruvbox henna
    homage-black homage-white horizon ir-black lantern laserwave manegarm
    material-dark material meltbus miramare molokai monokai-classic
    monokai-machine monokai-octagon monokai-pro monokai-ristretto
    monokai-spectrum moonlight nord-aurora nord-light nord nova oceanic-next
    old-hope one-light one opera-light opera outrun-electric palenight peacock
    pine plain-dark plain rouge shades-of-purple snazzy
    solarized-dark-high-contrast solarized-dark solarized-light sourcerer
    spacegrey tokyo-night tomorrow-day tomorrow-night vibrant wilmersdorf xcode
    zenburn))

(defun make-jl-doom-theme--make-face (face &optional skip-attrs)
  (let ((attrs
         (if (string-prefix-p "ansi-color-" (symbol-name face))
             '(:foreground)
           '(:height :weight :slant :foreground :background
             :underline :strike-through :inverse-video :inherit)))
        comma)
    (with-temp-buffer
      (insert "Face(")
      (dolist (attr attrs)
        (let ((val (face-attribute face attr)))
          (unless (memq val '(nil unspecified ()))
            (if comma
                (insert ", ")
              (setq comma t))
            (insert (pcase attr
                      (:strike-through "strikethrough")
                      (:inverse-video "inverse")
                      (_ (substring (symbol-name attr) 1)))
                    " = ")
            (pcase attr
              (:height
               (insert (format "%s" val)))
              ((or :weight :slant)
               (insert (replace-regexp-in-string
                        "-" "" (format ":%s" val))))
              ((or :strike-through :inverse-video)
               (insert (if val "true" "false")))
              ((or :foreground :background)
               (insert (apply 'format "0x%02x%02x%02x"
                              (mapcar (lambda (c) (ash c -8))
                                      (color-values val)))))
              (:underline
               (insert
                (pcase val
                  ('t "true")
                  ('nil "false")
                  ((or `(:color ,color :style ,style)
                       `(:style ,style :color ,color))
                   (format "(%s, :%s)"
                           (apply 'format "0x%02x%02x%02x"
                                  (mapcar (lambda (c) (ash c -8))
                                          (color-values color)))
                           (if (eq style 'wave) 'wavy style))))))
              (:inherit
               (insert
                "["
                (mapconcat
                 (lambda (v)
                   (format ":%s" (alist-get v make-jl-doom-theme--face-mapping)))
                 (ensure-list val)
                 ", ")
                "]"))))))
      (insert ")")
      (buffer-string))))

(defun make-jl-doom-theme (theme &optional no-restore-theme)
  (let* ((theme-name (format "%s" theme))
         (jl-theme-name  (replace-regexp-in-string
                          "[^0-9A-Za-z_]" ""
                          (subst-char-in-string ?- ?_ theme-name)))
         (jl-theme-upper (upcase jl-theme-name))
         (prev-theme doom-theme))
    (load-theme (intern (format "doom-%s" theme-name)) t)
    (with-temp-file (format "themes/%s.jl" theme-name)
      (unwind-protect
          (progn
            (insert (format "# Autogenerated from the doom-%s theme\n\n" theme-name)
                    (format "const _%s_FACES = [\n" jl-theme-upper))
            (dolist (face make-jl-doom-theme--face-mapping)
              (insert "  :" (cdr face) " => "
                      (if (car face)
                          (make-jl-doom-theme--make-face (car face))
                        "Face()")
                      ",\n"))
            (insert "]\n\n"
                    (format "THEMES[%s] = _%s_FACES"
                            (if (string-match-p "^[0-9]" jl-theme-name)
                                (format "Symbol(%S)" jl-theme-name)
                              (concat ":" jl-theme-name))
                            jl-theme-upper)
                    "\n"))
        (unless no-restore-theme
          (load-theme prev-theme t))))))

(defun make-jl-doom-all-themes ()
  (interactive)
  (let ((prev-theme doom-theme))
    (unwind-protect
        (dolist (theme make-jl-doom-themes--all)
          (make-jl-doom-theme theme))
      (load-theme prev-theme t))))

(provide 'make-jl-doom-theme)
;;; make-theme.el ends here
