#+title: DoomThemes.jl
#+author: tecosaur

A port of the [[https://github.com/doomemacs/themes][Doom (Emacs) Theme megapack]] to Julia, using its =Face= system.

* Usage

#+begin_src julia-repl
julia> DoomThemes.load(:theme_name)
#+end_src
